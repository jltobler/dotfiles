call plug#begin()
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'voldikss/vim-floaterm'

Plug 'preservim/nerdtree'

Plug 'ryanoasis/vim-devicons'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'Valloric/YouCompleteMe'
Plug 'editorconfig/editorconfig-vim'

Plug 'scheakur/vim-scheakur'

Plug 'jiangmiao/auto-pairs'
call plug#end()

let mapleader=","

set nocompatible " Disable compatibility with vi which can cause unexpected issues.
set noswapfile   " Disable swap file because I think they are annoying
set hidden       " Allow switching buffers without saving.
set autowrite
set encoding=UTF-8

au BufRead,BufNewFile *.md setlocal textwidth=80

" Disable arrow keys because I am noob.
noremap <Up>    <NOP>
noremap <Down>  <NOP>
noremap <Left>  <NOP>
noremap <Right> <NOP>

filetype on
filetype plugin on
filetype indent on

syntax on " Turn syntax highlighting on.

set ruler          " Set ruler in bottom right corner.
set cursorline     " Highlight cursor line underneath the cursor horizontally.
set number         " Add numbers to each line on the left-hand side.
set relativenumber " Make line numbers relative to current line.

set incsearch " Enable incremental search

colorscheme molokai
let g:airline_theme='dark_minimal'
let g:airline_powerline_fonts=1

" NERDTree configuration
nnoremap <silent> <leader>d :NERDTreeToggle<CR>
let NERDTreeShowHidden=1

" Window configuration
set fillchars+=vert:\  " Remove window border.

" go-vim configuration
" au filetype go inoremap <buffer> . .<C-x><C-o>
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_auto_type_info = 1
let g:go_doc_popup_window = 1 " Use new vim 8.2 popup windows for Go Doc.

" Set tab width to four spaces.
set tabstop=4
set shiftwidth=4
set smarttab

set showmatch " Show matching parenthesis.
hi MatchParen cterm=reverse

" floaterm configuration
nnoremap <silent> <leader>t :FloatermToggle<CR>
tnoremap <silent> <leader>t <C-\><C-n>:FloatermToggle<CR>

" fzf configuration
nnoremap <silent> <leader>b :Buffers<CR>
nnoremap <silent> <leader>f :Files<CR>
nnoremap <silent> <leader>F :Rg<CR>
nnoremap <silent> <leader>m :Mark<CR>

" devicons configuration
" after a re-source, fix syntax matching issues (concealing brackets):
if exists('g:loaded_webdevicons')
    call webdevicons#refresh()
endif

" YouCompleteMe configuration
set completeopt+=popup
set signcolumn=yes " Make Vim always render the sign column
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_max_diagnostics_to_display = 0
